# GSUPL-ChCore文档

说明: This is the repository of ChCore labs in OS, 2022 Autumin.


## 1. 配置GitLab仓库

Repository: https://gitlab.com/gsupl-chcore-lab/chcore-lab.git

Command line instructions

You can also upload existing files from your computer using the instructions below.

Git global setup
```
git config --global user.name "hcp6897"
git config --global user.email "hcp6897@163.com"
```

Create a new repository
```
git clone git@gitlab.com:gsupl-chcore-lab/chcore-lab.git
cd chcore-lab
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```

Push an existing folder
```
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:gsupl-chcore-lab/chcore-lab.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

Push an existing Git repository
```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:gsupl-chcore-lab/chcore-lab.git
git push -u origin --all
git push -u origin --tags
```


## 2. 实验环境配置

### 2.1 配置环境

1. 安装相关软件
```
sudo apt install gcc-aarch64-linux-gnu
sudo apt install make gdb-multiarch cmake ninja-build
sudo apt install qemu-system-aarch64
sudo apt install qemu-system-arm
sudo apt install qemu-efi-aarch64
sudo apt install qemu-utils
sudo apt install git
```

2. 安装 Docker
```
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
sudo docker run hello-world
```


### 2.2 编译问题说明

查看qemu支持的类型
```
qemu-system-aarch64 -machine help
```

修改Makefile文件

```
QEMUOPTS = -machine raspi3 -serial null -serial mon:stdio -m size=1G -kernel $(BUILD_DIR)/kernel.img -gdb tcp::1234
```
修改为:
```
QEMUOPTS = -machine raspi3b -serial null -serial mon:stdio -m size=1G -kernel $(BUILD_DIR)/kernel.img -gdb tcp::1234
```

### 2.3 实验执行
This is the repository of ChCore labs in SE315, 2020 Spring.

### 方法1：利用Makefile编译
### Build 
```
make or make build
```
The project will be built in `build` directory.

### 方法2：利用CMakeLists.txt编译

如果目录下已经存在 /build， 执行
```
make clean
```
之后，按照以下指令执行.

```
mkdir -p build
cd build
cmake -G Ninja ..
ninja
```

### Emulate
```
make qemu
```

Emulate ChCore in QEMU

### Debug with GBD
```
make qemu-gdb
```
Start a GDB server running ChCore
  
```
make gdb
```
  
Start a GDB (gdb-multiarch) client

### Grade
```
make grade
```

Show your grade of labs in the current branch

### Other
```
type `Ctrl+a x` to quit QEMU

type `Ctrl+d` to quit GDB

```


### Docker环境下编译执行
### Build
```
./scripts/docker_build.sh
```

### Emulate 
```
./scripts/run_docker.sh
```
